/*Implement quick sort algorithm in ascending way using Java. 
 * Provide input array of int numbers from console*/

package lab8;
import java.util.Arrays;
import java.util.Scanner;

public class Lab8 {
	public static void main(String[] args) {
		//input an array
		Scanner input = new Scanner(System.in);
		System.out.println("Number of integers in the array:");
		int n = input.nextInt();
		int[] arr = new int[n];
		System.out.println("Enter all of integers:");
		for (int i = 0; i < n; i++) {
			arr[i] = input.nextInt();
		}
		//sort array
		sort(arr, 0, n-1);
		System.out.println("Sorted array: " + Arrays.toString(arr));
	}
	
	 /*method to take last element as pivot, places the pivot element at its correct position
	  *  in sorted array, and places all smaller (smaller than pivot) to left of pivot and all
	  *  greater elements to right of pivot */
	public static int partition(int arr[], int low, int high) {
		int pivot = arr[high];
		int i = (low - 1); // index of smaller element
		for (int j = low; j < high; j++) {
			// If current element is smaller than or
			// equal to pivot
			if (arr[j] <= pivot) {
				i++;

				// swap arr[i] and arr[j]
				int temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
		}

		// swap arr[i+1] and arr[high] (or pivot)
		int temp = arr[i + 1];
		arr[i + 1] = arr[high];
		arr[high] = temp;

		return i + 1;
	}

	/*method to implement QuickSort() arr[] --> Array to be sorted,
	 * low --> Starting index, high --> Ending index*/
	public static int[] sort(int arr[], int low, int high) {
		if (low < high) {
			/* pi is partitioning index, arr[pi] is now at right place */
			int pi = partition(arr, low, high);

			// Recursively sort elements before partition and after partition
			sort(arr, low, pi - 1);
			sort(arr, pi + 1, high);
		}
		return arr;
	}
}
